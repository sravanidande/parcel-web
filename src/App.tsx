import 'hamburgers/_sass/hamburgers/hamburgers.scss'
import { Title } from 'devfractal-ui-core'
import React from 'react'
import { BurgerMenu } from './StaticBurger'
import { Burger } from './BurgerMenu'
import { CleaveTimeField } from './CleaveTimeField'

export const App = () => (
  <>
    <Title>helo burger</Title>
    <Title size="4">static</Title>
    <BurgerMenu />
    <Title size="4">Devfractal burger</Title>
    <Burger burgerType="elastic-r" />
    <Title size="4">Cleave Time Field</Title>
    <CleaveTimeField />
  </>
)
