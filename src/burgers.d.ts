declare module '@animated-burgers/burger-arrow' {
  import React from 'react'

  export type BurgerDirection = 'right' | 'left' | 'up' | 'down'

  export interface BurgerProps
    extends React.AllHTMLAttributes<React.AllHTMLAttributes> {
    readonly Component?: React.FC | string
    readonly isOpen?: boolean
    readonly direction?: BurgerDirection
  }
  function Burger(props: BurgerProps): JSX.Element
  export = Burger
}
