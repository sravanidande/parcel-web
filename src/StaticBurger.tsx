import React from 'react'

export const BurgerMenu = () => (
  <nav>
    <button
      className="hamburger hamburger--elastic"
      type="button"
      aria-label="Menu"
      aria-controls="navigation"
      aria-expanded="true"
    >
      <span className="hamburger-box">
        <span className="hamburger-inner"></span>
      </span>
    </button>

    <div id="navigation"></div>
  </nav>
)
