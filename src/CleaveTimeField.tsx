import React from 'react'
import { CleaveInputField, CleaveInput } from 'devfractal-forms'
import { Simple } from 'devfractal-simple'

export const CleaveTimeField = () => (
  <Simple.Form initialValues={{ time: '' }}>
    <CleaveInputField
      options={{ time: true, timePattern: ['h', 'm'] }}
      name="time"
    />
  </Simple.Form>
)
